# Maintainer: peippo <christoph+aur@christophfink.com>

pkgname=restic-scripts
pkgdesc="Simple systemd based scripts around restic"
url=

# first, configure restic (https://restic.readthedocs.io/en/latest/030_preparing_a_new_repo.html)

# then, copy /usr/share/doc/${pkgname}/restiv.env.example to
# ~/.config/restic.env and adopt the variables to reflect your
# set-up. 

# enable user systemd timers for each backup source directory
# ```
#    systemctl --user enable --now restic@$(systemd-escape /home/christoph/).timer
# ```

# if you want to expire old backups, also enable restic-expire
# ```
#    systemctl --user enable --now restic-expire.timer
# ```

pkgver=0.0.3
pkgrel=4

arch=("any")
license=("GPL")

depends=(
    "restic"
    "systemd"
)

source=(
    "expire-restic.sh"
    "restic.env.example"
    "restic-expire.service"
    "restic-expire.timer"
    "restic@.service"
    "restic@.timer"
    "logwatch-scripts-services-restic"
    "logwatch-conf-services-restic-expire.conf"
    "logwatch-conf-services-restic.conf"
)
sha256sums=("0f6db4e51df6adca425a4e526505373ca79cd94e8053e5acac35205b37b175f0"
            "a7db4d6813fef6270d2bc56fbed0315ec89fa9253b6a8f061190899acf3cd266"
            "6399048b498b0a9cc942430e1fe1485a792dc2e563f3a67b9c857563a4a4d849"
            "28dbd7726af8ed84a6dc546d14591c261ce37e3d38a8ef47dcfbfbf4f44f8346"
            "d86eeafff68bca0417a49a9c9e342b797ee4102b0cb0aff15403f99d794c86a1"
            "8f12b577e41012161b46a817b8a1cba9743fe10b9bd0c5bdc682027ba42314b4"
            "2fa8ef215b525b1d3c48ef087a3a97648bc9be4ed02bf4ab84733cd4b22b6bce"
            "bec37ee918c35a49f6ec7e0aa74b11db9dd30dbe4e982036b3db1aa903488aee"
            "c607b659a46133e0c7ab12fa5528838bc829815742ca722d4fe4ac676967dbbe")

package() {
    install \
        -Dm644 \
        -t "${pkgdir}/usr/lib/systemd/user/" \
        "restic-expire.service" \
        "restic-expire.timer" \
        "restic@.service" \
        "restic@.timer"

    install \
        -Dm644 \
        "restic.env.example" \
        "${pkgdir}/usr/share/doc/${pkgname}/restic.env.example"
   
    install \
        -Dm755 \
        "expire-restic.sh" \
        "${pkgdir}/usr/bin/expire-restic.sh" 

    install \
        -Dm755 \
        "logwatch-scripts-services-restic" \
        "${pkgdir}/usr/share/logwatch/scripts/services/restic" 

    ln -s \
        "restic" \
        "${pkgdir}/usr/share/logwatch/scripts/services/restic-expire"

    install \
        -Dm644 \
        "logwatch-conf-services-restic-expire.conf" \
        "${pkgdir}/usr/share/logwatch/dist.conf/services/restic-expire.conf"

    install \
        -Dm644 \
        "logwatch-conf-services-restic.conf" \
        "${pkgdir}/usr/share/logwatch/dist.conf/services/restic.conf"
}
